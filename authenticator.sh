#!/usr/bin/env sh

git clone git@gitlab.com:winniehell/acme-challenge.git
cd acme-challenge
echo $CERTBOT_VALIDATION > $CERTBOT_TOKEN
git add --all
git commit -m "Validation for ${DOMAIN}/.well-known/acme-challenge/${CERTBOT_TOKEN}"
git push
